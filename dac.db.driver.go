package gdac

import (
	"database/sql/driver"
	"fmt"
)

type DacDBDriver struct {
	libflag int8 // 0:unidac, 1:ado
	daclib  *DacLib
}

/*
*

	//    Provider=SQLOLEDB.1;Password=XXX;Persist Security Info=True;User ID=erp;Initial Catalog=twgpsdata;Data Source=101.34.50.22,10029
	ado:Provider=SQLOLEDB.1;Password=XXX;Persist Security Info=True;User ID=sa;Initial Catalog=KPERP_zssyj;Data Source=101.34.50.22,10002
	unidac:Provider Name=SQL Server;Login Prompt=False;Password=xxx;Data Source=101.34.50.22,10002;Initial Catalog=KPERP_zssyj;User ID=sa
*/
func (d *DacDBDriver) Open(dsn string) (driver.Conn, error) {
	strMap := make(map[string]string)
	if d.libflag == 1 { // ado
		ParseKVPairsEx(strMap, dsn, "=", ";", true)
		str := strMap["provider name"]
		if len(str) > 0 {
			dsn = fmt.Sprintf("Provider=SQLOLEDB.1;Persist Security Info=true;User ID=%s;Password=%s;Initial Catalog=%s;Data Source=%s",
				strMap["user id"], strMap["password"], strMap["initial catalog"], strMap["data source"])
		}
	}

	rval := NewDacDBConn(dsn, d.daclib)
	err := rval.open()
	if err != nil {
		return nil, err
	}
	return rval, nil
}
