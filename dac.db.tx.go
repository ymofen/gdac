package gdac

type DacDBDriverTx struct {
	daclib  *DacLib
	dacConn uintptr
}

func (this *DacDBDriverTx) Begin() error {
	return this.daclib.BeginTrans(this.dacConn)
}

func (this *DacDBDriverTx) Commit() error {
	return this.daclib.CommitTrans(this.dacConn)
}

func (this *DacDBDriverTx) Rollback() error {
	return this.daclib.RollbackTrans(this.dacConn)
}
