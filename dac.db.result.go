package gdac

import "errors"

type DacDBResult struct {
	n int64
}

func (r *DacDBResult) LastInsertId() (int64, error) {
	return 0, errors.New("LastInsertId not supported")
}

func (r *DacDBResult) RowsAffected() (int64, error) {
	return r.n, nil
}
