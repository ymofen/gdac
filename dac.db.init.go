package gdac

import (
	"database/sql"
	"fmt"
)

var (
	DacLibIntf_Uni *DacLib
	DacLibIntf_ADO *DacLib
)

func StatusString() string {
	return fmt.Sprintf("Uni:%s, Ado:%s", DacLibIntf_Uni.StatusString(), DacLibIntf_ADO.StatusString())
}

func init() {
	DacLibIntf_Uni = NewDacLib("libDac.dll")
	DacLibIntf_ADO = NewDacLib("libADO.dll")
	sql.Register("dacdb", &DacDBDriver{daclib: DacLibIntf_Uni})
	sql.Register("dacdb-unidac", &DacDBDriver{daclib: DacLibIntf_Uni})
	sql.Register("dacdb-ado", &DacDBDriver{daclib: DacLibIntf_ADO, libflag: 1})
}
