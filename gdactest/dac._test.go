package gdactest

import (
	"database/sql"
	"fmt"
	_ "gitee.com/ymofen/gdac"
	"strconv"
	"testing"
	"time"
)

func TestUnidac(t *testing.T) {
	db, err := sql.Open("dacdb-unidac", "Provider Name=SQL Server;Login Prompt=False;Password=sa;Data Source=127.0.0.1,2433;Initial Catalog=dac;User ID=sa")
	if err != nil {
		fmt.Println("open", err)
		return
	}
	defer db.Close()
	//db.Exec("select * from abc")

	{
		tmpsql := `drop table __dac_testtable`
		_, err := db.Exec(tmpsql)
		if err != nil {
			//t.Fatal(err)
		}
	}

	{
		tmpsql := `CREATE TABLE __dac_testtable (
  fid int  IDENTITY(1,1) NOT NULL,
  fstr varchar(100) NULL,
  fint int NULL,
  fdatetime datetime NULL,
  ftimestamp timestamp NULL,
)`
		_, err := db.Exec(tmpsql)
		if err != nil {
			t.Fatal(err)
		}
	}

	t0, _ := time.ParseInLocation("2006-01-02 15:04:05", "2023-09-15 01:01:01", time.Local)
	strval := "hello 中国!"
	intval0 := 65535000
	{
		tmpsql := fmt.Sprintf(`insert into __dac_testtable(fstr, fint, fdatetime) select '%s', %d, '%s'`,
			strval, intval0, t0.Format("2006-01-02 15:04:05"))
		_, err := db.Exec(tmpsql)
		if err != nil {
			t.Fatalf("sql：%s, err:%s", tmpsql, err.Error())
		}
	}

	{

		tmpsql := `select * from __dac_testtable`
		rows, err := db.Query(tmpsql)
		if err != nil {
			t.Fatalf("sql：%s, err:%s", tmpsql, err.Error())
		}

		rowval := make([]interface{}, 5)
		scanArgs := make([]interface{}, len(rowval))
		for i := 0; i < len(rowval); i++ {
			scanArgs[i] = &rowval[i]
		}
		if !rows.Next() {
			t.Fatal("没有记录")
			return
		}
		rows.Scan(scanArgs...)

		{
			str := rowval[1]
			if str != strval {
				t.Fatalf("get fstr err:%s", str)
			}
		}

		{
			intv, _ := strconv.ParseInt(rowval[2].(string), 10, 64)
			if int(intv) != intval0 {
				t.Fatalf("get fint err:%d", intv)
			}
		}

		{
			str := rowval[3]
			if str != t0.Format("2006-01-02 15:04:05") {
				t.Fatalf("get fdatetime err:%s", str)
			}
		}

	}

}
